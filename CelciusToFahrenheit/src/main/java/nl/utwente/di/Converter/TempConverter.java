package nl.utwente.di.Converter;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;


/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class TempConverter extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Converting convert;

    public void init() throws ServletException {
        convert = new Converting();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Celcius to Fahrenheit Converter";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celcius: " +
                request.getParameter("temperature") + "\n" +
                "  <P>Fahrenheit: " +
                Double.toString(convert.getFahrenheit(request.getParameter("temperature"))) +
                "</BODY></HTML>");
    }


}
