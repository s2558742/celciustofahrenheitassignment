package nl.utwente.di.Converter;

public class Converting {

    public double getFahrenheit(String temp) {
        double res = 0;
        double celcius = Double.parseDouble(temp);

        res = (celcius * 9.0/5.0) + 32.0;


        return res;
    }

}
